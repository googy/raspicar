import io
import random
import picamera

import picamera.array
import numpy as np



import logging
import socketserver
import threading
from http import server
import time




PAGE="""\
<html>
<head>
<title>picamera MJPEG streaming demo</title>
</head>
<body>
<h1>PiCamera MJPEG Streaming Demo</h1>
<img src="stream.mjpg" width="1296" height="400" />
</body>
</html>
"""

class StreamingOutput(object):
    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = threading.Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)

class StreamingHandler(server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.send_response(301)
            self.send_header('Location', '/index.html')
            self.end_headers()
        elif self.path == '/index.html':
            content = PAGE.encode('utf-8')
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Content-Length', len(content))
            self.end_headers()
            self.wfile.write(content)
        elif self.path == '/stream.mjpg':
            self.send_response(200)
            self.send_header('Age', 0)
            self.send_header('Cache-Control', 'no-cache, private')
            self.send_header('Pragma', 'no-cache')
            self.send_header('Content-Type', 'multipart/x-mixed-replace; boundary=FRAME')
            self.end_headers()
            try:
                while True:
                    with output.condition:
                        output.condition.wait()
                        frame = output.frame
                    self.wfile.write(b'--FRAME\r\n')
                    self.send_header('Content-Type', 'image/jpeg')
                    self.send_header('Content-Length', len(frame))
                    self.end_headers()
                    self.wfile.write(frame)
                    self.wfile.write(b'\r\n')
            except Exception as e:
                logging.warning(
                    'Removed streaming client %s: %s',
                    self.client_address, str(e))
        else:
            self.send_error(404)
            self.end_headers()

class StreamingServer(socketserver.ThreadingMixIn, server.HTTPServer):
    allow_reuse_address = True
    daemon_threads = True







class MyMotionDetector(picamera.array.PiMotionAnalysis):
# Funktion wird staendig mit motion Daten gefuettert, welche im encoder beim codieren der Videodaten entstehen
# siehe motion_output
    def analyse(self, a):
        global mov
        a = np.sqrt(
            np.square(a['x'].astype(np.float)) +
            np.square(a['y'].astype(np.float))
            ).clip(0, 255).astype(np.uint8)
        # If there're more than 10 vectors with a magnitude greater
        # than 60, then say we've detected motion
        if (a > 60).sum() > 10:
            motion_sem.release()
            print('mo')






camera = picamera.PiCamera()
camera.resolution = (1296, 400)
#camera.framerate = 5

# stream used to save Video to Flash
stream = picamera.PiCameraCircularIO(camera, seconds=10)
camera.start_recording(stream, format='h264', motion_output=MyMotionDetector(camera))	# motion kostet ca. 20% Rechenzeit

# split video stream to mjpeg for live streaming
output = StreamingOutput()
camera.start_recording(output, format='mjpeg', splitter_port=2)	# kostet ca. 15% Rechenzeit

# create and start server in a Thread
address = ('', 8000)
server = StreamingServer(address, StreamingHandler)
threading.Thread(target=server.serve_forever, daemon=True).start()	# server kostet ca. 5% Rechenzeit



motion_sem = threading.Semaphore()

seq = 0



#def cam_controller():
#    camera = picamera.PiCamera()
#    camera.resolution = (1296, 400)
#    stream = picamera.PiCameraCircularIO(camera, seconds=10)
#    output = StreamingOutput()
#    if record
#        camera.start_recording(stream, format='h264', motion_output=MyMotionDetector(camera))   # motion kostet ca. 20% Rechenzeit
#    if stream
#        camera.start_recording(output, format='mjpeg', splitter_port=2) # kostet ca. 15% Rechenzeit



try:
    while True:
        time.sleep(5)
        motion_sem.acquire()
        print('Motion detected___')
        # from now on record to flash
        # TODO split if file too big
        camera.split_recording('after%d.h264' % seq)

        # copy recorded video from buffer to flash
        stream.copy_to('bevore%d.h264' % seq, seconds=10)	# kostet kurzzeitig ca. 5% Rechenleistung, paar Sekunden

        seq += 1
#       stream.clear()
        time.sleep(10)
        # Wait until motion is no longer detected, then split
        # recording back to the in-memory circular buffer

#        while mov == True:
#            camera.wait_recording(2)
#        print('Motion stopped!')
        camera.split_recording(stream)
finally:
    camera.stop_recording()
