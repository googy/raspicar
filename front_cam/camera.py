import picamera		# Raspberry Pi Camera
import threading	# thread
import time

class CamCon(threading.Thread):
    def __init__(self, stopEvent, recordingEvent, annotation, annotation_lock):
        threading.Thread.__init__(self)
        self.stopEvent = stopEvent
        self.recordingEvent = recordingEvent
        self.annotation = annotation
        self.annotation_lock = annotation_lock
        self.tim = 0

    def run(self):	# camera control thread
        self.camera = picamera.PiCamera()
        self.camera.led = False		# sets GPIO library to Broadcom (BCM) mode with GPIO.setmode(GPIO.BCM) and disables warnings with GPIO.setwarnings(False). LED cannot be controlled when the library is in BOARD mode.
        recording = False
        ti = 0
        ann = ""
        while not self.stopEvent.is_set():	# rec service running
            if self.recordingEvent.is_set() and not recording:	# start recording
                print "start recording"
                self.camera.resolution = (1296, 400)
                #self.camera.resolution = (1296, 730)	#full FOV
                #self.camera.resolution = (1920, 1080)	#HD
                self.camera.hflip = True
                self.camera.vflip = True
                self.camera.framerate = 24
                self.camera.annotate_foreground = picamera.Color('white')
                self.camera.annotate_background = picamera.Color('black')
                self.camera.annotate_text_size = 24	#range 6 to 160, inclusive. The default is 32.
                self.tim = int(time.time())	#time in sec since epoch
                self.camera.start_recording('ramdisk/v%d.h264' % self.tim)
                recording = True
            if recording:		# while recording
                self.annotation_lock.acquire()
                if not self.annotation.empty():
                    ann = self.annotation.get()
                self.annotation_lock.release()
                if (int(time.time()) - self.tim) > 60:
                    self.tim = int(time.time())
                    self.camera.split_recording('ramdisk/v%d.h264' % self.tim)
                if len(ann) > 2:
                    self.camera.annotate_text = ann
                    ann = ""
            if recording and not self.recordingEvent.is_set():	# stop recording
                recording = False
                self.camera.stop_recording()	# stop camera
                print "stop"
        if recording:		# shutdown thread while still recording
            self.camera.stop_recording()	# stop camera
        #camera.wait_recording(10)
        #camera.capture('foo.jpg', use_video_port=True)
        return
