import threading
import shutil						# copy files
import os						# list files
import time						# current time


class FileService(threading.Thread):
    def __init__(self, stopEvent, source, destination):
        threading.Thread.__init__(self)
        self.stopEvent = stopEvent
        self.source = source
        self.destination = destination

    def run(self):
        while not self.stopEvent.is_set():
            # check if disk not full, if so remove old files
            df = os.popen("df --output=avail")		# read free disk space using df
            df.readline()				# skip first line of output containing column titles
            if int(df.readline()) < 10000000:		# ckeck if availiable space is less then threshold
                fil = os.listdir(self.destination)
                fil.sort()
                vids = []
                # ignore non-video files
                for file in fil:
                    if file.endswith(".h264"):
                         vids.append(file)
                # delete two oldest files
                if len(vids) > 2:
                    os.remove(self.destination + vids.pop(0))
                    os.remove(self.destination + vids.pop(0))

            # move files from ramdisk to flash
            files = os.listdir(self.source)		# list all files in Folder
            files.sort()					# sort files by name, no needed
            for file in files:					# iterate over all files
                if file.endswith(".h264"):				# ignore all files that are not videos
                    # Alter der Datei bestimmen, falls die Datei eine Zeit lang nicht modifiziert worden ist, so ist diese Datei fertig und kann gesichert bzw. verarbeitet werden
                    statinfo = os.stat(os.path.join(self.source, file))			# get file info
                    filetime = int(statinfo.st_ctime)		# st_mtime: Time of most recent content modification expressed in seconds
                    curtime = int(time.time())			# get current time in seconds
                    if (curtime - filetime) > 70:			# if the file has not been modified for a number of seconds
                     # die Wartezeit kann aus dem vorhandenen RAMdisk Platz und der Datenrate berechnet werden. Aktuell werden die Videodateien, welche laenger als 10 Sekundennicht mehr modifiziert worden sind verschoben. Das heisst zur gleichen Zeit befindet sich eine volle Datei von 60s und eine Datei mit ca. 10s Video. Also muss die Ramdisk fuer min. 70s Video reichen. Sollte die Datenrate oder die Groesse der Ramdisk sich aendern, so muss auch die Laenge der Videabschnitte angepasst werden. Laenge Videoabschnitt, Datenrate und Ramdisk Groesse muessen zueinander passen!
                        shutil.move(os.path.join(self.source, file), os.path.join(self.destination, file))
                        print ("move", curtime - filetime)

            # slow down unnessecery checking
            time.sleep(1)

            # Thread soll nicht staendig alle Dateien abklappern, sondern nur viel seltener
            # da aktuell nur alle 60s es neue Dateien gibt. Daher soll der Thread schlafen,
            # bis es was zu tun gibt.
            # Das event stopEvent ist dafuer vorgesehen, um den thread stoppen zu koennen
            # das Problem ist nun aber, wenn der Thread schlaeft, kann dieser nicht
            # angehalten werden. So muss der Thread zuende schlagen und erst dann wird
            # stopEvent geprueft. Diese Wartezeit ist viel zu lang.
            # um das ganze etwas zu beschleunigen kann man eine condition.wait with defined timeout, if timeout is over
