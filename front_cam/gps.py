class GPS:
    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.sentence = []

    def parseGPStime(self, time):
        return time[0:2] + ":" + time[2:4] + ":" + time[4:6]

    def addCharacter(self, c):
        if ord(c) == 13:
            erg = self.parseNMEA(self.sentence)
            if erg is not None:
                return erg
        if c == '$':
            self.sentence = []
        self.sentence.append(c)
        return None

    def checksum(self, sentence):
        checksum = int(''.join(sentence[-2:]), 16)
        value = 0;
        for c in sentence[1:-3]:
            value ^= ord(c);
        if value == checksum:
            return True
        else:
            return False

    def parseRMC(self, sentence):
        st = ''.join(sentence)
        elements = st.split(",")
        if elements[2] == "V":
            return None
        if elements[2] == "A":
            return self.parseGPStime(elements[1]) +" "+elements[4]+elements[3]+" "+elements[6]+elements[5]+" "+ '%.2f' % (float(elements[7]) / 0.539956803456)+"km/h"

    def parseNMEA(self, sentence):
        if len(sentence) > 5:
            if sentence[0] == '$':
                #f = open("gps",'w')
                #f.write(sentence) # python will convert \n to os.linesep
                #f.close() # you can omit in most cases as the destructor will call it
                if (sentence[1] == 'G') and (sentence[2] == 'P'):
                    if self.checksum(sentence):
                        if sentence[3] == 'R' and sentence[4] == 'M' and sentence[5] == "C":
                            return self.parseRMC(sentence)
