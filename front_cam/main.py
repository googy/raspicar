import picamera		# Raspberry Pi Camera
import serial		# serial port
import threading	# thread
import signal		# Ctrl+C signal handler
import sys		# exit
import gps		# GPS service
from Queue import Queue	# to transfer data between threads
import RPi.GPIO as GPIO
import smbus		# I2C
import time
import oled		# OLED lib
import camera		# camera.py Camera control Thread
import os
from files import FileService	# files transfer service

port = serial.Serial("/dev/ttyAMA0", baudrate=9600, timeout=3.0)
gps = gps.GPS(0, 0)		# GPS handling object


# UART thread, should be moved to separate file
def worker(queue, stop_event):		# GPS worker thread, TODO move to GPS service module
    port.flushInput()       		# flush buffer bevore read
    while not stop_event.is_set():
        c = port.read(1)
        str = gps.addCharacter(c)		# read one character from uart and process
        if str is not None:		# there is an usefull result
            print(str)
            queue.put(str)		# write result to memory
    return

def signal_handler(signal, frame):
    print('Ctrl+C! pressed')
    terminating.set()		# initialize gracefull exit of whole process

# setup GPS Thread
terminating = threading.Event()		# used to signal process termination
gpsStopEvent = threading.Event()	# used to signal termination to the GPS service thread
q = Queue()
lock = threading.Lock()			# locks data transfer beween main and GPS threads
t = threading.Thread(target=worker, args=(q, gpsStopEvent))
t.start()

# setup Camera Thread
cameraStopEvent = threading.Event()	# used to signal termination to the Camera service thread
recordingEvent = threading.Event()
annotation_lock = threading.Lock()
annotation = Queue()
k = camera.CamCon(cameraStopEvent, recordingEvent, annotation, annotation_lock)
k.start()

# setup File Thread
fileStopEvent = threading.Event()
sourceDir = "/home/pi/raspicar/front_cam/ramdisk/"
destinationDir = "/home/pi/raspicar/front_cam/video/"
fileService = FileService(fileStopEvent, sourceDir, destinationDir)
fileService.start()

signal.signal(signal.SIGINT, signal_handler)		# install signal handler for SIGINT

############################################GPIO####################
GPIO.setmode(GPIO.BCM)
# PUD_UP, PUD_DOWN or PUD_OFF (default)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_OFF)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_OFF)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_OFF)
GPIO.setup(25, GPIO.IN, pull_up_down=GPIO.PUD_OFF)
####################################################################

##########################OLED######################################
oledd = oled.OLED(0x3c)
oledd.init()
oledd.clear()
####################################################################

menu = ["record      ","stop      ","shutdown      "]
nr = 0
oledd.print_string(menu[nr], 5, 0)
pressed_23 = False
pressed_24 = False

while True:
    if not q.empty():		# get GPS data
        print "GPS data availiable"
        e = q.get(False)		# try to get, no wait
        annotation_lock.acquire()
        annotation.queue.clear()
        annotation.put(e)
        annotation_lock.release()
        tim = time.gmtime()
        print e, tim.tm_year, tim.tm_mon, tim.tm_mday,tim.tm_hour, tim.tm_min, tim.tm_sec
        oledd.print_string("GPS: " + str(e[0:8]), 0, 0)			# print GPS time to OLED, first line

#    if GPIO.input(22):			# left outer button pressed

## 23
    if GPIO.input(23) and not pressed_23:
        pressed_23 = True
        nr = nr + 1
        if nr >= len(menu):
            nr = 0
        oledd.print_string(menu[nr], 5, 0)			# print GPS time to OLED, first line
    if pressed_23 and not GPIO.input(23):
        pressed_23 = False
## 24
    if GPIO.input(24) and not pressed_24:			# right inner button pressed
        pressed_24 = True
        nr = nr - 1
        if nr < 0:
            nr = len(menu) - 1
        oledd.print_string(menu[nr], 5, 0)			# print GPS time to OLED, first line
    if pressed_24 and not GPIO.input(24):
        pressed_24 = False
        
#       oledd.print_string(str(time.gmtime().tm_hour) + ":" + str(time.gmtime().tm_min) + ":" + str(time.gmtime().tm_sec), 1, 0)	# print System time to OLED, second line
    if GPIO.input(25):			# right outer button pressed
        if nr == 0:
            recordingEvent.set()
            oledd.print_string('recording ', 4, 0)
        if nr == 1:
            recordingEvent.clear()
            oledd.print_string('stopped   ', 4, 0)
        if nr == 2:
            oledd.clear()
            GPIO.cleanup()			# free GPIO
            os.system('shutdown now -h')

    if GPIO.input(22):
        print "trigger"

    if terminating.is_set():
        print "stopping"
        gpsStopEvent.set()		# let GPS service thread terminate
        cameraStopEvent.set()
        fileStopEvent.set()
        GPIO.cleanup()			# free GPIO
        t.join()			# wait for GPS service thread to terminate
        k.join()
        fileService.join()
        oledd.clear()
        sys.exit(0)			# exit process
